import { Body, Controller, Delete, Get, Param, Post, Put } from '@nestjs/common';
import { catchError, map, Observable, of } from 'rxjs';
import { User } from '../models/user.interface';
import { UserService } from '../service/user.service';

@Controller('users')
export class UserController {
    constructor(private readonly userService:UserService){}
    
    /*For @Post
    create(@Body()user:User):Observable<User> 
    - we extract user from the body which is type :User and we return an Observable which is also type User
    we return this.userServuce.create(user) and we give it to a user
    */
    @Post()
    create(@Body()user:User):Observable<User | Object>{
        return this.userService.create(user).pipe(
            map((user:User)=> user),
            catchError(err => of({error: err.message}))
        );
    }

    @Post('login')
    login(@Body() user:User):Observable<Object>{
        return this.userService.login(user).pipe(
            map((jwt:string) => {
                return {access_token: jwt};
            })
        )
    }

    @Get(":id")
    findAll(@Param()params):Observable<User>{
        return this.userService.findOne(params.id)
    }

    @Get()
    findOne():Observable<User[]>{
        return this.userService.findAll();
    }

    @Delete(':id')
    deleteOne(@Param('id')id:string):Observable<User>{/*Type string since it comes from out URL */
        return this.userService.deleteOne(Number(id))//By Number(id) we convert an ID to a number        
    }

    @Put(':id')
    updateOne(@Param("id")id:string, @Body()user:User):Observable<any>{
        return this.userService.updateOne(Number(id),user)
    }
}
