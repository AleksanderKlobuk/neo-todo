import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthModule } from 'src/auth/auth.module';
import { UserController } from './controller/user.controller';
import { UserEntity } from './models/user.entity';
import { UserService } from './service/user.service';

@Module({
  imports: [
    AuthModule,
    TypeOrmModule.forFeature([UserEntity])],//use typeorm for users feature created in UserEntity
  providers: [UserService],
  controllers: [UserController]
})
export class UserModule {}
