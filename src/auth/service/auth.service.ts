import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { of,from, Observable } from 'rxjs';
import { User } from 'src/user/models/user.interface';
const bcrypt = require('bcrypt')

@Injectable()
export class AuthService {
    constructor(private readonly jwtSecret:JwtService){}

    generateJWT(user:User):Observable<string>{
        return from(this.jwtSecret.signAsync({user})) //generator JWT
    }

    hashPassword(password:string):Observable<string>{
        return from<string>(bcrypt.hash(password,12)) //hash password
    }

    comparePasswords(newPassword:string, passwordHash:string ):Observable<any | boolean >{
        return of<any | boolean >(bcrypt.compare(newPassword,passwordHash))
    }
}
